<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Nyholm\Psr7\Response as NyholmResponse;

use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Rsa\Sha256;

require __DIR__ . '/../vendor/autoload.php';

function getUnauthorizedResponse(string $message)
{
    $resonse = new NyholmResponse();
    $resonse->getBody()->write(json_encode(array(
        'message' => $message
    )));
    return $resonse
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Content-Type', 'application/json')
        ->withStatus(401);
}

$app = AppFactory::create();

$app->options('/', function (Request $request, Response $response, $args) {
    return $response
        ->withHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization')
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Methods', 'GET, OPTIONS');
});

$app->get('/', function (Request $request, Response $response, $args) {
    // Get token string from Authorizarion header
    $bearer = $request->getHeader('Authorization');
    $data = explode(' ', $bearer[0]);
    if ($data[0] !== 'Bearer') {
        return getUnauthorizedResponse(
            'HTTP 401: Authorization must contain a string with the following format -> Bearer JWT'
        );
    }

    // Parse the JWT Token
    $token = (new Parser())->parse((string) $data[1]);

    // Validating token (verifying expiration date and issuer)
    $data = new ValidationData();
    if (!$token->validate($data)) {
        return getUnauthorizedResponse('HTTP 401: Access Token is not valid or has expired');
    }

    // Test token signature with the public key
    $publicKey = new Key('file:///project/public_key');
    if (!$token->verify(new Sha256(), $publicKey)) {
        return getUnauthorizedResponse('HTTP 401: Access Token signature is not valid');
    }

    $response->getBody()->write(json_encode(array(
        'pain au chocolat ou chocolatine', 'cassoulet', 'ravioli'
    )));
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Content-Type', 'application/json');
});

$app->run();
