UID := 1000
GID := 1000

list:
	@echo ""
	@echo "Useful targets:"
	@echo ""
	@echo "  rebuild        > rebuild all images and start containers for dev only"
	@echo "  start          > start containers"
	@echo "  restart        > restart containers"
	@echo "  stop           > stop and kill running containers"
	@echo "  status         > display stack containers status"
	@echo "  logs           > display containers logs"
	@echo "  install-webapp > install webapp dependencies"
	@echo "  install-api    > install api dependencies"
	@echo "  shell-webapp   > shell into angular client container"
	@echo "  shell-api      > shell into php api container"
	@echo "  remove-pgdata  > remove the pgdata databases"
	@echo ""

rebuild:
	@docker-compose up --build -d

start:
	@docker-compose up -d

restart: stop start

stop:
	@docker-compose kill
	@docker-compose rm -v --force

status:
	@docker-compose ps

logs:
	@docker-compose logs -f -t

install-webapp:
	@docker build -f ./conf-dev/angular-cli.dockerfile -t angular-cli . && docker run --init -it --rm --user $(UID):$(GID) \
	-v $(CURDIR)/webapp:/project \
	-w /project angular-cli yarn install

install-api:
	@docker run --init -it --rm --user $(UID):$(GID) \
	-e COMPOSER_CACHE_DIR=/dev/null \
	-v $(CURDIR)/api:/project \
	-w /project jakzal/phpqa:php7.4 composer install --ignore-platform-reqs

shell-webapp:
	@docker-compose exec webapp bash

shell-api:
	@docker-compose exec api bash

remove-pgdata:
	@docker volume rm keycloak_pgdata