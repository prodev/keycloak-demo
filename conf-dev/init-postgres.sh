#!/bin/sh
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE USER keycloak LOGIN PASSWORD 'keycloak';
    CREATE DATABASE keycloakdb;
EOSQL