# KEYCLOAK pour ProDev

## Prérequis

Avant de commencer l'installation, assurez-vous d'avoir les commandes suivantes installées sur votre système :

- `make`
- `docker`

Une connexion internet sera nécessaire afin de télécharger les dépendances.

## Installation et démarrage de l'application en mode développement

Le projet contient un Makefile qui aide à l'installation et au démarrage de l'application.

1. Pour installer la webapp et ces dépendances, taper à la racine du projet :

> make install-webapp

2. Pour installer l'API et ces dépendances, taper à la racine du projet :

> make install-api

3. Lancer l'application :

> make start

4. Stopper l'application :

> make stop

## Les services

 - Keycloak est disponible à l'adresse : http://localhost:8180
 - La webapp est disponible à l'adresse : http://localhost:4200
 - L'API est disponible à l'adresse : http://localhost:8080

## Liste des commandes

Le fichier `Makefile` situé à la racine du projet rend disponible une liste de commandes utiles à la gestion de
l'application en mode développement.

Pour voir la liste des commandes disponibles, ouvrir un terminal à la racine du projet et taper :

> make

## Supprimer la base de données

Pour supprimer la base de données keycloak et repartir à 0, ouvrir un terminal à la racine du projet et taper :

> make stop
> make remove-pgdata
> make start

**Attention** : ces commandes sont destinées à être utilisées en mode développement uniquement !

## Auteurs

* `François Agneray` : Laboratoire d'Astrophysique de Marseille (CNRS)