import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { ToastrService } from 'ngx-toastr';
import { KeycloakService } from 'keycloak-angular';
import { KeycloakProfile } from 'keycloak-js';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public isLoggedIn = false;
  public userProfile: KeycloakProfile | null = null;
  public accessToken: string = null;
  public refreshToken: string = null;
  public liste = null;

  constructor(
    private toastr: ToastrService,
    private readonly keycloak: KeycloakService, 
    private http: HttpClient
  ) { }

  public async ngOnInit() {
    this.isLoggedIn = await this.keycloak.isLoggedIn();

    if (this.isLoggedIn) {
      this.userProfile = await this.keycloak.loadUserProfile();
      this.accessToken = this.keycloak.getKeycloakInstance().token;
      this.refreshToken = this.keycloak.getKeycloakInstance().refreshToken;
    }
  }

  public login() {
    this.keycloak.login();
  }

  public logout() {
    this.keycloak.logout();
  }

  public getList() {
    this.http.get<any>('http://localhost:8080/').subscribe(data => {
        this.toastr.success('Retrieve list done', 'Success');
        this.liste = data;
    }, (err: HttpErrorResponse) => {
      if (err.status == 401) {
        this.toastr.warning('Failed to retrieve list (401)', 'Unauthorized');
      } else {
        this.toastr.error('Failed to retrieve list', 'HTTP Error');
      }
    })
  }
}
